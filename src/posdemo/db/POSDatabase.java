package posdemo.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import posdemo.model.Sale;

public class POSDatabase implements Serializable {

	List<Sale> sales = new ArrayList<Sale>();
	
	private POSDatabase() { 	}
	
	// --------- Singleton implementation --------------
	
	private static POSDatabase instance;
	
	public static POSDatabase instance() {
		if (instance == null)
			instance = new POSDatabase();
		
		return instance;
	}
}
