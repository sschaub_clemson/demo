package posdemo.ui;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTextPane;

public class SaleFrame extends JFrame {
	private JTextField txtItem;
	private JTextField txtQuantity;
	
	public SaleFrame() {
		getContentPane().setLayout(null);
		
		JLabel lblItem = new JLabel("Item:");
		lblItem.setBounds(30, 43, 46, 14);
		getContentPane().add(lblItem);
		
		JLabel lblQuantity = new JLabel("Quantity:");
		lblQuantity.setBounds(185, 43, 46, 14);
		getContentPane().add(lblQuantity);
		
		txtItem = new JTextField();
		txtItem.setBounds(65, 40, 86, 20);
		getContentPane().add(txtItem);
		txtItem.setColumns(10);
		
		txtQuantity = new JTextField();
		txtQuantity.setBounds(241, 40, 86, 20);
		getContentPane().add(txtQuantity);
		txtQuantity.setColumns(10);
		
		JButton btnAddItem = new JButton("Add Item");
		btnAddItem.setMnemonic('A');
		btnAddItem.setBounds(359, 39, 89, 23);
		getContentPane().add(btnAddItem);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(49, 104, 384, 131);
		getContentPane().add(textPane);
	}
}
