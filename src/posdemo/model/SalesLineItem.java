package posdemo.model;

import java.io.Serializable;

public class SalesLineItem implements Serializable {
	private int quantity;
	private ProductSpecification spec;
	
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public ProductSpecification getSpec() {
		return spec;
	}
	public void setSpec(ProductSpecification spec) {
		this.spec = spec;
	}
	
}
