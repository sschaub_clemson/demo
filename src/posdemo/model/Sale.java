// Sale class represents a Sale
// Some more stuff
package posdemo.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

// I think I'll comment this way
public class Sale implements Serializable {
	private Date dateTime; // date and time
	private List<SalesLineItem> items; // items in sale
	
	public Date getDateTime() {
		// an update
		return dateTime;
	}
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	public List<SalesLineItem> getItems() {
		return items;
	}
	public void setItems(List<SalesLineItem> items) {
		this.items = items;
	}
} 
